var s,
App = {
	settings: {
		oscFreq: null,
		oscPeriod: null,
		pwmPeriod: null,
		pwmFreq: null,
		pwmActualFreq: null,
		pr2: null,
		prescaler: null,
		maxResolution: null,
		sourceCode: "",
		dutyCycleMaxVal: null,
		dutyCycle: null
	},
	init: function() {
		s = this.settings;

		this.handleValidation();
	},
	setTimer2: function (prsc) {
		if (prsc == 1)
			return "00000100";
		else if (prsc == 4)
			return "00000101";
		else if (prsc == 16)
			return "00000111";
		else
			return "error";
	},
	binaryToString: function (binary) {
		var i = 0, result = "";

		for (i=0; i<8; i++) {
			if (binary & 1<<i)
				result = "1" + result;
			else
				result = "0" + result;
		}
		return result;
	},
	setValueById: function (id, value) {
		var obj = document.getElementById(id);
		obj.innerHTML = value ;
	},
	calculatePWM: function () {
		this.clearData();

		if (isNaN(document.pwmForm.DutyCycle.value))
			alert("Warning: Duty value must be number");
		else
			s.dutyCycle = document.pwmForm.DutyCycle.value;

		if (s.dutyCycle < 0 || s.dutyCycle > 100)
		{
			alert("Warning: Duty percent must be number 0-100");
			return;
		}

		s.oscFreq = document.pwmForm.osc_freq.value;
		s.pwmFreq = document.pwmForm.PWM_Freq.value;

		if (isNaN(s.oscFreq) || isNaN(s.pwmFreq)) {
			alert("Warning: Oscillator and PWM Frequencies must be numbers");
			return;
		}

		s.oscPeriod = 1/s.oscFreq;
		s.pwmPeriod = 1/s.pwmFreq;

		for (s.prescaler = 1; s.prescaler <=16; s.prescaler *= 4) {
			s.pr2 = Math.round((s.pwmPeriod)/(s.prescaler * s.oscPeriod * 4)-1);  // calculate a s.pr2 value

			if (s.pr2 > 255 || s.pr2 < 1)  // only values between 255 and 1 will be allowed
			{
				if (s.pr2 > 255)
					this.setValueById("PR2_" + s.prescaler , "Too high");
				else
					this.setValueById("PR2_" + s.prescaler , "Too low");
			} else {
				s.pwmActualFreq = 1/((s.pr2 + 1) * s.oscPeriod * 4 * s.prescaler);    // determine the actual PWM frequency with these settings
				s.dutyCycleMaxVal = Math.round(s.pwmPeriod / (s.oscPeriod * s.prescaler));  //calculate the maximum duty value that this prescale allows

				s.dutyCycle = Math.round(s.dutyCycle/100 * s.dutyCycleMaxVal);

				this.setValueById("PR2_" + s.prescaler , "(" + s.pr2 + ") 0b" + this.binaryToString(s.pr2));
				this.setValueById("ActualPWM_" + s.prescaler , Math.round(s.pwmActualFreq));
				this.setValueById("T2CON_" + s.prescaler , "0b" + this.setTimer2(s.prescaler));
				this.setValueById( "CCPR1L_" + s.prescaler, "0b" + this.binaryToString(s.dutyCycle >> 2));
				this.setValueById("CCP1CON_" + s.prescaler,  "0b" +  this.binaryToString( ((s.dutyCycle & 3) << 4) + 0x0C));
				this.setValueById("MaxDuty_" + s.prescaler, s.dutyCycleMaxVal);

				//create the source code output
				s.sourceCode += "/*\n";
				s.sourceCode += " * PWM Register Values\n";
				s.sourceCode += " * Oscillator Frequency Fosc = " + s.oscFreq + "Hz\n";
				s.sourceCode += " * Clock Frequency Fclk = " + s.oscFreq/4 + "Hz\n";
				s.sourceCode += " * PWM Freq = " + s.pwmFreq + "Hz desired...actual: " + Math.round(s.pwmActualFreq) +  "Hz\n";
				s.sourceCode += " * Prescaler Value = " + s.prescaler + "\n";
				s.sourceCode += " * PR2 = " + s.pr2 + "\n";
				s.sourceCode += " * Maximum duty value = " + s.dutyCycleMaxVal + " \n";
				//sc += " * Resolution (bits) = " + maxResolution + " \n";
				s.sourceCode += " * Requested Duty Value = " + s.dutyCycle + "\n";
				s.sourceCode += " * \n";
				s.sourceCode += " * Code Provided AS IS....Use at your own risk!\n";
				s.sourceCode += " */\n\n"

				s.sourceCode += "// be sure to set PWM port as output\n";
				s.sourceCode += "T2CON = 0b" + this.setTimer2(s.prescaler) + "; // prescaler + turn on TMR2;\n";
				s.sourceCode += "PR2 = 0b" + this.binaryToString(s.pr2) + ";\n";
				s.sourceCode += "CCPR1L = 0b" + this.binaryToString(s.dutyCycle >> 2)  + ";  // set duty MSB\n";
				s.sourceCode += "CCP1CON = 0b" + this.binaryToString( ((s.dutyCycle & 3) << 4) + 0x0C) + "; // duty lowest bits + PWM mode\n";   // and it with 3 then shiftit over 44 bits into the 4 and fifth bit add 12 to put in pwm mode
			}
		}

		document.getElementById('sourceCode').innerHTML = s.sourceCode;
	},
	clearData: function () {
		for (s.prescaler = 1; s.prescaler <= 16; s.prescaler *=4) {
			this.setValueById("PR2_" + s.prescaler , "");
			this.setValueById("PR2_" + s.prescaler , "");
			this.setValueById("ActualPWM_" + s.prescaler , "");
			this.setValueById("T2CON_" + s.prescaler , "");
			this.setValueById( "CCPR1L_" + s.prescaler, "");
			this.setValueById("CCP1CON_" + s.prescaler,  "");
			this.setValueById("MaxDuty_" + s.prescaler, "");
		};
		document.getElementById('sourceCode').innerHTML = "";
		s.sourceCode = "";
	},
	handleValidation: function () {
		var $form = $('#pwmForm');
		var $error = $('.alert-danger', $form);

		jQuery.validator.setDefaults({
		  debug: true,
		  success: "valid"
		});

		$form.validate({
			//errorElement: 'span', //default input error message container
			//errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			rules: {
				'osc_freq': {
					min: 1,
					number: true,
					required: true
				},
				'PWM_Freq': {
					min: 1,
					number: true,
					required: true
				},
				'DutyCycle': {
					range: [0, 100],
					required: true
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				$error.toggleClass('hidden');
				window.scrollTo($error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				$error.hide();
				App.calculatePWM();
			}
		});
	}
}

$(document).ready(function () {
	App.init();
});